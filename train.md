# train from 0
    ./darknet detector train yolo/annotate/obj.data yolo/yolo-obj-train.cfg -gpus 0,1


# train from check point
    ./darknet detector train yolo/annotate/obj.data yolo/yolo-obj-train.cfg backup/yolo-obj-train.backup -gpus 0,1

# command
## gpu monitor
    watch -n 1 nvidia-smi
### cpu monitor
    htop
    watch -n 1 sensors